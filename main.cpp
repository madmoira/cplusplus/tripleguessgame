#include <iostream>
#include <random>
#include <chrono>
#include <boost/format.hpp>

inline void PrintIntroduction(int LevelDifficulty) {
    std::cout << boost::format{"You are a secret agent breaking into a level %s secure server room \n"}
                 % LevelDifficulty;
    std::cout << "You need to enter the correct codes to continue...\n";
}

bool PlayGame(int LevelDifficulty) {
    PrintIntroduction(LevelDifficulty);
    const int64_t seed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    std::default_random_engine engine(seed);
    std::uniform_int_distribution<int> distribution(1, LevelDifficulty + 1);

    const int CodeA = distribution(engine);
    const int CodeB = distribution(engine);
    const int CodeC = distribution(engine);

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProduct = CodeA * CodeB * CodeC;

    std::cout << "+ There are 3 numbers in the code\n";
    std::cout << boost::format{"+ The codes add-up to: %s\n"} % CodeSum;
    std::cout << boost::format{"+ The codes multiply to give:  %s \n"} % CodeProduct;

    int GuessA = 0, GuessB = 0, GuessC = 0;
    std::cin >> GuessA;
    std::cin >> GuessB;
    std::cin >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;
    std::cout << boost::format{"+ Your guesses add-up to: %s\n"} % GuessSum;
    std::cout << boost::format{"+ Your guesses multiply to: %s\n"} % GuessProduct;

    if (GuessSum == CodeSum && GuessProduct == CodeProduct) {
        std::cout << "You guessed the correct code, you moved to the next server room!!\n\n";
        return true;
    } else {
        std::cout << "You failed to decipher the code, be careful, try again\n\n";
        return false;
    }
}

int main() {
    bool bLevelComplete;
    int LevelDifficulty = 1;
    const int MaxDifficulty = 5;

    while (true) {
        bLevelComplete = PlayGame(LevelDifficulty);
        std::cin.clear();
        std::cin.ignore();

        if (LevelDifficulty >= MaxDifficulty && bLevelComplete) {
            std::cout << "You won, you broke into all the sever rooms and got all the files.";
            break;
        }

        if (bLevelComplete) {
            ++LevelDifficulty;
        }
    }

    return 0;
}